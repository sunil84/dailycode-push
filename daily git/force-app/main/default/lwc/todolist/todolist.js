import { LightningElement ,track } from 'lwc';
const columns = [
  {label:'Name',fieldname:'name'},
  {label:'Email',fieldname:'email'},
  {label:'Phone',fieldname:'phone'}
];
export default class Todolist extends LightningElement {


    TodoId = 2;
    @track selectrowid  =1;
    @track todos = [
        { id: 1, taskDetails: 'Write Story' },
        { id: 2, taskDetails: 'Build Framework' }
    ];
   
    @track taskDetails;
    handleToDoChanges(event){
        this.taskDetails = event.target.value;
   
    }
    handleToDo(){
      this.TodoId = this.TodoId + 1;
        this.todos = [
            ...this.todos,
            {
                id: this.TodoId,
                taskDetails: this.taskDetails,
            }
        ];
    }

    selectrow(event){
        
        this.selectrowid = event.target.dataset.id;
        console.log('log>>>>>>',this.selectrowid);
    }
}