import { LightningElement, track ,wire} from 'lwc';

import getApplication from '@salesforce/apex/AccountDataController.getApplication';

export default class Document extends LightningElement {


@track multiple = true;
@track accounts ;
@track application ;
@track searchValue ;
@track contacts;
  @track selectedOption;
  searchKey = '';

@track showvalue = true;

handleClick(){
  if(this.showvalue == true){
    this.showvalue = false
    console.log('OUTPUT : ',this.showvalue);
  }else if(this.showvalue == false){
    this.showvalue = true    
    
    console.log('OUTPUT1 : ',this.showvalue);
  }
}  
    

  get options() {
    return [
             { label: 'Type 1', value: 'Type 1' }
             
           ];
}

 get options1() {
    return [
             { label: 'Type 2', value: 'Type 2' }
             
           ];
}

  handleChange(event) {
        this.selectedOption = event.detail.value;
   

       console.log("gggggh",this.selectedOption);
     getApplication({ accountName: this.selectedOption})
            .then(result => {
                this.contacts = result;
          
                console.log('accountName',this.contacts);
            })
            .catch(error => {
                this.error = error;
               
            });
    }

   
}