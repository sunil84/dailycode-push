import { LightningElement ,api, track,wire} from 'lwc';
import createTask from '@salesforce/apex/TaskLwc.createTask'
import TaskList from '@salesforce/apex/TaskLwc.TaskList'
import completedTaskList from '@salesforce/apex/TaskLwc.completedTaskList'
import unCompleteTaskList from '@salesforce/apex/TaskLwc.unCompleteTaskList'
import deleteTask from '@salesforce/apex/TaskLwc.deleteTask'
import SizeTask from '@salesforce/apex/TaskLwc.SizeTask'
import updateTask from '@salesforce/apex/TaskLwc.updateTask'
import { refreshApex } from '@salesforce/apex';
const actions = [
    { label: 'Delete', name: 'delete' },
];

const columns = [
    { label: 'Subject', fieldName: 'Subject' },
    { label: 'Priority', fieldName: 'Status'},
    { label: 'Type', fieldName: 'Type'},
    { label: 'Status', fieldName: 'Status'},
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];
export default class TodoList2 extends LightningElement {

   columns = columns;

    @track Description;
    @track Priority;
    @track Type;
    @track Subject;
    @track FilterValue;
    @track allTask=false;
    @track completed=false;
    @track notCompleted=false;
    @track TaskRecordList;
    @track CompletedTaskRecordList;
    @track NotCompletedTaskRecordList;
    @track allSize;
    @track completeSize;
    @track notCompleteSize;

    @track error;

    @wire(SizeTask)wiredSizeTask({data , error}){
        if(data){
            this.allSize = data[0];
            this.completeSize = data[1];
            this.notCompleteSize = data[2];
        }else if(error){
            this.error = error;
        }

    };

    get TypePicklistValue(){
        return[
            {label:"Call",value:"Call"},
            {label:"Meeting",value:"Meeting"},
            {label:"Other",value:"Other"}
        ];
    }

    get PriorityPicklistValue(){
        return[
            {label:"High",value:"High"},
            {label:"Normal",value:"Normal"},
            {label:"Low",value:"Low"}
        ];
    }

    get getFilterValue(){
        return[
            {label:"AllTask",value:"AllTask"},
            {label:"Completed",value:"Completed"},
            {label:"Not Completed",value:"NotCompleted"}
        ];
    }

    HandleFilterChange(event){
        this.FilterValue = event.target.value;
        console.log(this.FilterValue);

        if(this.FilterValue == 'AllTask'){
            // this.TaskRecordList.pop();
            this.completed = false;
            this.allTask = true;
             this.notCompleted = false;
            //refreshData();
            TaskList()
            .then(task => {
                 this.TaskRecordList = task;
                 console.log('this.TaskRecordList    ',this.TaskRecordList);
            })
            .catch(error =>{
                console.log('error  ',error);
            });

        }else 
        if(this.FilterValue == 'Completed'){
            //  this.TaskRecordList.pop();
            this.completed = true;
            this.allTask = false;
             this.notCompleted = false;
            //refreshData();
            completedTaskList()
            .then(task => {
                 this.CompletedTaskRecordList = task;
                 console.log('this.CompletedTaskRecordList    ',this.CompletedTaskRecordList);
            })
            .catch(error =>{
                console.log('error  ',error);
            });

        }else if(this.FilterValue == 'NotCompleted'){
            // this.TaskRecordList.pop();
             this.completed = false;
            this.allTask = false;
             this.notCompleted = true;
            //refreshData();
            unCompleteTaskList()
            .then(task => {
                 this.NotCompletedTaskRecordList = task;
                 console.log('this.NotCompletedTaskRecordList    ',this.NotCompletedTaskRecordList);
            })
            .catch(error =>{
                console.log('error  ',error);
            });

        }
    }

    handleDescriptionChange(event){
        this.Description = event.target.value;
        console.log(this.Description);
    }
    HandlePriorityChange(event){
        this.Priority = event.target.value;
        console.log(this.Priority);
    }
    HandleTypeChange(event){
        this.Type = event.target.value;
        console.log(this.Type);
    }
    handleChange(event){
        this.Subject = event.target.value;
        console.log(this.Subject);
    }

    addTask(){
        createTask({Subject:this.Subject , Description:this.Description , Priority:this.Priority , Type:this.Type})
        .then(task => {
            console.log('Task  ',task);
            this.Subject = '';
            this.Description = '';
            this.Priority = '';
            this.Type = '';
        })
        .catch(error => {
            console.log('error   ',error);
        });
    }

    handleRowAction(event){
        const action = event.detail.action.name;
        const details = event.detail.row;

        console.log('action   ',action);
        console.log('details  ',details.Id);

        if(action == 'delete'){
            deleteTask({taskId : details.Id});
            

        }
        


    }

    handleClickAction(){
        var selectedRecords =  this.template.querySelector("lightning-datatable").getSelectedRows();
        console.log('selectedRecords---',selectedRecords);
        selectedRecords.forEach(currentItem => {
            //   ids = ids + ',' + currentItem.Id;
              updateTask({taskId : currentItem.Id});

          });
    }

    refreshData() {
        return refreshApex(this.TaskRecordList);
    }
    

}