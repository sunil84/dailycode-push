import { LightningElement, track, wire,api } from 'lwc';
import retrieveAccounts from '@salesforce/apex/AccountDataController.retrieveAccounts';
import { NavigationMixin } from 'lightning/navigation';
const actions = [
    { label: 'View', name: 'view' },
    { label: 'Edit', name: 'edit' },
     { label: 'contact', name: 'contact' },
];

const columns = [
    { label: 'Name', fieldName: 'Name' },
    { label: 'Type', fieldName: 'Type' },
    { label: 'BillingCountry', fieldName: 'BillingCountry'},   
      {   label: 'Action',
        type: 'action',
        initialWidth:'50px',
        typeAttributes: { rowActions: actions },
    },      
    
];

export default class Datatable extends LightningElement {

    @api content;
   @track modaal = false;
    @track data;
    @track columns = columns;
    @track error;
 
    @wire(retrieveAccounts)
    wiredAccounts({ error, data }) {
        if (data) {
            this.data = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.data = undefined;
        }
    }
 
    getSelectedRec() {
      var selectedRecords =  this.template.querySelector("lightning-datatable").getSelectedRows();
      if(selectedRecords.length > 0){
          console.log('selectedRecords are ', selectedRecords);
 
          let ids = '';
          selectedRecords.forEach(currentItem => {
              ids = ids + ',' + currentItem.Id;
          });
          this.selectedIds = ids.replace(/^,/, '');
          this.lstSelectedRecords = selectedRecords;
          alert(this.selectedIds);
      }   
    }

    
    handleRowAction(event) {
        const actionName = event.detail.action.name;
        console.log('actionName',actionName);
        

        if(actionName == View)
        {
            console.log('op');
            modaal = true;
        }
     
    }

    
    handleOkay() {
        this.close('okay');
    }
}