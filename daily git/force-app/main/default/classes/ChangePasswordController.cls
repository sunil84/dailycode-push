/**
 * An apex page controller that exposes the change password functionality
 */

 
public with sharing class ChangePasswordController {

      public static List<Account> getAccounts(){
        
        list<account> acclist =[select  Id,phone, name,accountnumber from account];
        return acclist;
    }
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        
    
    public PageReference changePassword() {
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
    }     
    
   	public ChangePasswordController() {}
}