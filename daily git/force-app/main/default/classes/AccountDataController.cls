public class AccountDataController {


    // apex method to fetch contact records from salesforce database 
    @AuraEnabled
    public static list<Contact> fetchContacts(){        
            list<Contact> lstContact = new list<Contact>();
            for(Contact con : [SELECT id,name,email,phone,title FROM Contact LIMIT 5000]){
                lstContact.add(con);
            } 
            return lstContact;        
    }
     @AuraEnabled(cacheable=true)
  public static List<Account> getAccounts() {
  return [SELECT Name, Phone,Id FROM Account];
  }
      @AuraEnabled (cacheable=true)
    public static List<Account> retrieveAccounts(){
        return [SELECT Id, Name, Type, BillingCountry
                FROM Account];
    }

       
    public static void getAccountData() {
        string accountname = 'Type 2';
       List <Application__c> appList = [SELECT Id, Name ,typeValue__c, Years__c from Application__c where  typeValue__c =:accountname];
        system.debug('aapList'+appList);
    }

      @AuraEnabled
    public static List<Application__c> getApplication(String accountName) {
     System.debug('accountName'+accountName);
         string accountname1 = accountName;
        
       
        return [SELECT Id, Name ,typeValue__c, Years__c from Application__c where  typeValue__c =:accountName];
    }


   
    @AuraEnabled
    public static Task createTask(String Subject , String Description , String Priority , string Type){
        Task taskRecord = new Task();
        taskRecord.Subject = Subject;
        taskRecord.Description = Description;
        taskRecord.Type = Type;
        taskRecord.Priority = Priority;
        taskRecord.OwnerId = UserInfo.getUserId();
        taskRecord.ActivityDate = System.today();
        insert taskRecord;
        return taskRecord;
    }

    @AuraEnabled
    public static List<Task> TaskList(){
        Id userId = UserInfo.getUserId();
        return [Select id , subject,Priority,Type,status from Task where OwnerId =:userId ];
    }

    @AuraEnabled
    public static List<Task> completedTaskList(){
        Id userId = UserInfo.getUserId();
        return [Select id , subject,Priority,Type ,status from Task where OwnerId =:userId and status = 'Completed'];
    }

    @AuraEnabled
    public static List<Task> unCompleteTaskList(){
        Id userId = UserInfo.getUserId();
        return [Select id , subject,Priority,Type,status from Task where OwnerId =:userId and status != 'Completed'];
    }

     @AuraEnabled
    public static void deleteTask(Id taskId){

        delete([Select id from task where id =: taskId]);

    }

     @AuraEnabled(cacheable=true)
    public static List<Integer> SizeTask(){
        Id userId = UserInfo.getUserId();
        Integer Total = [Select id , subject,Priority,Type,status from Task where OwnerId =:userId].size();
        Integer Completed = [Select id , subject,Priority,Type,status from Task where OwnerId =:userId and status = 'Completed'].size();
        Integer NOTCompleted = [Select id , subject,Priority,Type,status from Task where OwnerId =:userId and status != 'Completed'].size();
        List<Integer> sizeList = new List<Integer>();
        sizeList.add(Total);
        sizeList.add(Completed);
        sizeList.add(NOTCompleted);
        return sizeList;
    }

     @AuraEnabled
    public static void updateTask(Id taskId){

        Task t = [Select id from task where id =: taskId];
        t.status = 'Completed';
        update t;

    }


}